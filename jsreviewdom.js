window.onload = () =>{

// 1.1 Basandote en el array siguiente, crea una lista ul > li
// dinámicamente en el html que imprima cada uno de los paises.

//     const iteraccion1 = () => {

// const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

// const body = document.querySelector('body');
// const ul = document.createElement('ul');
// body.appendChild(ul);

// countries.forEach(element => {
//     const li = document.createElement('li');
//     li.textContent=element;
//     ul.appendChild(li);
// });

// }

// iteraccion1();

// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.

// const iteraccion2=()=>{

// const element = document.querySelector('.fn-remove-me');
// element.remove();

// }

// 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos
// en el div de html con el atributo data-function="printHere".

// const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

// const parent = document.querySelector('div[data-function="printHere"]')
// const ul = document.createElement('ul');
// parent.appendChild(ul);

// cars.forEach(element => {
//     const li = document.createElement('li');
//     li.textContent=element;
//     ul.appendChild(li);
// });


// 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento
// h4 para el titulo y otro elemento img para la imagen.
// const countries = [
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
// ];

//     const body = document.querySelector('body');

//     countries.forEach(element => {
//         const divContainer = document.createElement('div');
//         const title= document.createElement('h4');
//         const img = document.createElement('img');

//         title.textContent=element.title;
//         img.src=element.imgUrl;

//         body.appendChild(divContainer);
//         divContainer.appendChild(title);
//         divContainer.appendChild(img);

//     });

// 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último
// elemento de la lista.

// const iteraccion5 = () => {

// const countries = [
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
// ];

//     const body = document.querySelector('body');
//     const ul = document.createElement('ul');
//     body.appendChild(ul);

//     countries.forEach(element => {
//         const divContainer = document.createElement('div');
//         const title= document.createElement('h4');
//         const img = document.createElement('img');

//         title.textContent=element.title;
//         img.src=element.imgUrl;

//         ul.appendChild(divContainer);
//         divContainer.appendChild(title);
//         divContainer.appendChild(img);
//     });

//     const btnToDelete = document.createElement('button');
//     btnToDelete.innerHTML='delete here';
//     body.appendChild(btnToDelete);

//     const deleteElement = () => {
//         var elementToDelete = ul.lastChild;
//         elementToDelete.remove();
//     }

//     btnToDelete.addEventListener('click', deleteElement)

// }

// iteraccion5();

// 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los
// elementos de las listas que elimine ese mismo elemento del html.

// const iteraccion6 = () => {

//     const countries = [
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
// 	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
// ];

// const body = document.querySelector('body');
// const ul = document.createElement('ul');
// body.appendChild(ul);

//     countries.forEach(element => {
//         const divContainer = document.createElement('div');
//         const title= document.createElement('h4');
//         const img = document.createElement('img');
//         const btnToDelete = document.createElement('button');

//         title.textContent=element.title;
//         img.src=element.imgUrl;
//         btnToDelete.innerHTML='delete here';

//         ul.appendChild(divContainer);
//         divContainer.appendChild(title);
//         divContainer.appendChild(img);
//         divContainer.appendChild(btnToDelete);

//     const deleteElement = () => {
//         btnToDelete.parentNode.remove();
//     }

//     btnToDelete.addEventListener('click', deleteElement)
//     });
// }

// iteraccion6();

}

